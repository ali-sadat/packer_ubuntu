#!/bin/bash

# install packages
sudo apt-get update
sudo apt-get -y install \
     apt-transport-https \
     ca-certificates \
     curl \
     sshpass \
     software-properties-common \
     python3-pip \
     libltdl7 \
     ifupdown \
     python-minimal \


