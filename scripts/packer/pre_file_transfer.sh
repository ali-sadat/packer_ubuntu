#!/bin/sh

set -e
set -x

#sudo mkdir -p /home/vagrant/x01
#sudo mkdir -p /home/vagrant/x02/
#sudo mkdir /home/vagrant/x03
#sudo mkdir /home/vagrant/x04/

#sudo mkdir -p /home/x05
#sudo mkdir -p /home/x06/

sudo mkdir -p /home/vagrant/downloadCache/packages
sudo chmod 700 /home/vagrant/downloadCache/packages
sudo chown -R vagrant:vagrant /home/vagrant/downloadCache/packages
