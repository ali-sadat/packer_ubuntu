#!/bin/sh

set -e
set -x

date | sudo tee /etc/vagrant_box_build_time

sudo mkdir -p ~/.ssh
sudo touch ~/.ssh/authorized_keys
#sudo curl -fsSLo ~/.ssh/authorized_keys https://raw.githubusercontent.com/iprimo/ssh_key/master/id_rsa.pub
sudo curl -fsSLo ~/.ssh/authorized_keys https://raw.githubusercontent.com/hashicorp/vagrant/master/keys/vagrant.pub
sudo chmod 700 ~/.ssh/
sudo chmod 600 ~/.ssh/authorized_keys
sudo chown -R vagrant:vagrant ~/.ssh
sudo -S sh -c "echo 'vagrant ALL=NOPASSWD: ALL' >> /etc/sudoers"
sudo echo 'vagrant ALL=NOPASSWD:ALL' > /etc/sudoers.d/vagrant

#sudo touch /etc/sudoers_test
#sudo -S sh -c "echo 'vagrant ALL=NOPASSWD: ALL' >> /etc/sudoers_test"
#sudo touch /etc/sudoers.d/vagrant_test
#sudo echo 'vagrant ALL=NOPASSWD:ALL' > /etc/sudoers.d/vagrant_test

