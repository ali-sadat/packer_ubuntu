# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.

# Ensure yaml module is loaded
require 'yaml'
require 'securerandom'

# Read yaml node definitions to create
# **Update nodes.yml to reflect any changes
current_dir    = File.dirname(File.expand_path(__FILE__))
nodes_content  = YAML.load_file("#{current_dir}/nodes.yml")

nodes = nodes_content['nodes_config']

# Vagrantfile API/syntax version. Don't touch unless you know what you're doing!
VAGRANTFILE_API_VERSION = '2' #.freeze
Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  
  # Iterate over nodes
  nodes.each do |node_id|

    #config.ssh.forward_agent = true
    #config.ssh.username = "vagrant"  
    #config.ssh.password = "vagrant"  
    #config.ssh.insert_key = false
    #config.ssh.private_key_path = File.expand_path("../../keys/vagrant_private_key", __FILE__)


    # This condition determines if all nodes have to be created or only one
    unless nodes_content['deploy'] == 'all'
      next unless nodes_content['deploy'] == node_id['name']
    end

    config.vm.box = 'ubuntu-18.04.1-server-amd64-virtualbox'
    config.vm.box_url = '../../artifacts/packer/ubuntu-18.04.1-server-amd64-virtualbox.box'
    


    # Below is needed if not using Guest Additions
    # config.vm.synced_folder ".", "/vagrant", type: "rsync",
    #   rsync__exclude: "hosts"
    config.vm.define node_id['name'] do |node|


      node_id['synced_folder'].each do |sync_folder|
        if sync_folder['mount'] == true
          unless sync_folder['type'].nil?
            config.vm.synced_folder sync_folder['host_folder'], \
                                 sync_folder['guest_folder'], \
                                 type: sync_folder['type']
            $sync_folder_glob_var = sync_folder['guest_folder']
          end
        end
      end

      #node.vm.box = node_id['box']
      node.vm.hostname = node_id['name']
      node.vm.provider :virtualbox do |vb|
        vb.name = node_id['name']
        vb.cpus = node_id['vcpu']
        vb.memory = node_id['mem']
        vb.linked_clone = true
        
        vb.customize ['setextradata', 'global', 'GUI/SuppressMessages', 'all' ]
        vb.customize ['modifyvm', :id, '--ioapic', 'on']
        #vb.customize ["modifyvm", :id, "--vram", "128"]
        #vb.customize ["modifyvm", :id, "--clipboard", "bidirectional"]
        
        # Add additional disk(s)
        unless node_id['disks'].nil?
          #vb.customize ['storagectl', :id, '--name', 'IDE', '--remove']
          #vb.customize ['storagectl', :id, '--name', 'SCSI', '--add']
          controller = 'SCSI Controller'
          vb.customize ["storagectl", :id, "--name", "#{controller}", "--add", "SCSI"]
          # Get disk path
          line = `VBoxManage list systemproperties | grep "Default machine folder"`
          vb_machine_folder = line.split(':')[1].strip()

          dnum = 4
          node_id['disks'].each do |disk_num|
            dnum = (dnum.to_i + 1)
            ddev = File.join(vb_machine_folder, vb.name, "#{node_id['name']}_Disk#{dnum}.vmdk")
            dsize = disk_num['size'].to_i * 1024
            unless File.exist?(ddev.to_s)
              vb.customize ['createhd', '--filename', ddev.to_s, \
                            #'--variant', 'Fixed',\
                             '--size', dsize]
            end
            vb.customize ['storageattach', :id, '--storagectl', \
                          (disk_num['controller']).to_s, '--port', dnum, '--device', 0, \
                          '--type', 'hdd', '--medium', ddev.to_s]
          end
        end

        # Provision network interfaces
        unless node_id['interfaces'].nil?
          node_id['interfaces'].each do |int|
              if int['type'] == 'private_network'
                  node.vm.network "private_network", \
                      type: "dhcp", \
                      :mac => '0090CF' + SecureRandom.hex(2) + '01' # MAC address ending with "1" is for HostOnly connection and uses DHCP

              elsif int['type'] == 'public_network'
                  if int['method'] == 'dhcp'
                      # Put some interface DHCP config here,
                  elsif int['method'] == 'static'
                      unless int['bridge'].nil?
                          node.vm.network "public_network", \
                              bridge: int['bridge'], \
                              ip: int['ip'], \
                              netmask: int['netmask'], \
                              :mac => '0090CF' + SecureRandom.hex(2) + '02'
                       end
                   end
               end
           end
         end
        end

#        unless node_id['windows'].nil?
#          # runs initial script
#          if node_id['windows']
#            node.vm.provision 'shell', path: 'scripts/ConfigureRemotingForAnsible.ps1'
#          else
#            #node.vm.provision "shell", path: 'scripts/bootstrap.sh' , keep_color: 'false' # keep_color (boolean) - Vagrant automatically colors output in green and red depending on whether the output is from stdout or stderr. If this is true, Vagrant will not do this, allowing the native colors from the script to be outputted.
#            node.vm.provision "ansible" do |ansible|
#              #ansible.verbose = "v" 
#              ansible.limit = 'all'
#              ansible.playbook = "playbooks/vagrant/playbook.yml"
#            end
#          end
#        end


     end
   end
end

